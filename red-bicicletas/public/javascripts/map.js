var map = L.map('main_map').setView([6.176783, -75.641083], 18);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// L.marker([6.176783, -75.641083]).addTo(map);
// L.marker([6.176960, -75.642150]).addTo(map);
// L.marker([6.177900, -75.641045]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, {title: bici.code}).addTo(map);
        });
    }
})
