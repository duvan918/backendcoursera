var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function (req, res) {
    Bicicleta.allBicis(function(err, bicis){
        res.render('bicicletas/index', {bicis});
    });
};
// Forma con exec()
// exports.bicicleta_list = function (req, res) {
//     Bicicleta.allBicis().exec((err, bicis) => {
//         res.render('bicicletas/index', {bicis});
//     })  
// };

exports.bicicleta_create_get = function(req, res) {
    res.render('bicicletas/create');
};

exports.bicicleta_create_post = function(req, res) {
    var bici = new Bicicleta({
        code: req.body.code, 
        color: req.body.color, 
        modelo: req.body.modelo,
        ubicacion: [req.body.lat || 0, req.body.lng || 0]
    });
    // Otra forma: Sin pasar CallBack - Se puede ya que Bicicleta.add() no retorna ningun valor
    // Bicicleta.add(bici);
    // res.redirect('/bicicletas');
    Bicicleta.add(bici, function(){
        res.redirect('/bicicletas');
    });
};

exports.bicicleta_update_get = function(req, res) {
    Bicicleta.findByCode(req.params.code, function(err, bici){
        res.render('bicicletas/update', {bici});
    });
};

exports.bicicleta_update_post = function(req, res) {
    var setUpdate = { 
        code : req.body.code,
        color : req.body.color,
        modelo: req.body.modelo,
        ubicacion : [req.body.lat, req.body.lng]
    };
    Bicicleta.updateByCode(req.params.code, setUpdate, () => {
        res.redirect('/bicicletas');
    });

};

exports.bicicleta_delete_post = function(req, res) {
    Bicicleta.removeByCode(req.body.code, function(){
        res.redirect('/bicicletas');
    });
};