var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res) {
    Bicicleta.allBicis(function(err,bicis){
        res.status(200).json({ bicicletas: bicis });
    });
};

exports.bicicleta_create = function(req, res) {
    var bici = new Bicicleta({
        code: req.body.code, 
        color: req.body.color, 
        modelo: req.body.modelo,
        ubicacion: [req.body.lat || 0, req.body.lng || 0]
    });
    Bicicleta.add(bici, function(){
        res.status(200).json({ bicicleta: bici });
    });

    
};

exports.bicicleta_update = function(req, res) {
    var setUpdate = { 
        code: req.body.code,
        color : req.body.color,
        modelo: req.body.modelo,
        ubicacion : [req.body.lat, req.body.lng]
    };
    Bicicleta.updateByCode(req.body.code, setUpdate, (err, bici) => {
        res.status(200).json({ bicicleta: bici });
    });
    // ¿Acaso es mejor usar Patch para esta parte?
    // ¿Acaso es mejor usar Patch para modificar solo un pedazo?
    // if (req.body.code) bici.code = req.body.code;
    // if (req.body.color) bici.color = req.body.color;
    // if (req.body.modelo) bici.modelo = req.body.modelo;
    // if (req.body.lat) bici.ubicacion[0] = req.body.lat;
    // if (req.body.lng) bici.ubicacion[1] = req.body.lng;
};

exports.bicicleta_delete = function(req, res) {
    Bicicleta.removeByCode(req.body.code, function(){
        res.status(200).send('Eliminado');
    });
};