var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function() {
    beforeEach(function(done) {
        mongoose.disconnect();

        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if(err) console.log(err);
            done();
        });
    });

    beforeAll((done) => {
        mongoose.connection.close(done);
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, 'verde', 'urbana', [6.177400, -75.641445]);
            
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(6.177400);
            expect(bici.ubicacion[1]).toEqual(-75.641445);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);//has to be empty at the begining
                done();
            });
        });
    });  

    describe('Bicicleta.add', () => {
        it('agregamos una bicicleta', (done) => {
            var aBici = new Bicicleta({code: 1, color: 'verde', modelo: 'urbana'});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: 'verde', modelo: 'urbana'});
                Bicicleta.add(aBici, function(err, newBici) {
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: 'roja', modelo: 'urbana'});
                    Bicicleta.add(aBici2, function(err, newBici) {
                        if(err) console.log(err);
                        Bicicleta.findByCode(1, function(error, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('debe eliminar la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: 'verde', modelo: 'urbana'});
                Bicicleta.add(aBici, function(err, newBici) {
                    if(err) console.log(err);
                    Bicicleta.allBicis(function(err, bicis) {
                        expect(bicis.length).toBe(1);
                        Bicicleta.removeByCode(1, function(error, bicis) {
                            Bicicleta.allBicis(function(err, bicis) {
                                expect(bicis.length).toBe(0);
                                
                                done();
                            });
                        });
                    });
                });
            });
        });
    });
});

/*
// beforeEach(() => {console.log('testeando…');});  // Hace parte de las preguntas del curso
// beforeEach(function(){console.log(‘testeando…’); }); // Forma aceptada en la evaluación
// beforeEach(() => Bicicleta.allBicis = []);   // No se puso porque al correr "npm test" tambien borraba allBicis en las pruebas API

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        Bicicleta.allBicis = [];
        expect(Bicicleta.allBicis.length).toBe(0);
    })
});

describe('Bicicleta.add', () => {
    it('agregamos una bicicleta', () => {
        Bicicleta.allBicis = [];
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [6.177400, -75.641445]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    })
});

describe('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        Bicicleta.allBicis = [];
        expect(Bicicleta.allBicis.length).toBe(0);

        var aBici1 = new Bicicleta(1, 'verde', 'urbana');
        var aBici2 = new Bicicleta(2, 'verde', 'urbana');
        Bicicleta.add(aBici1);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici1.color);
        expect(targetBici.modelo).toBe(aBici1.modelo);
    })
});

describe('Bicicleta.removeById', () => {
    it('debe remover la bici con id 1', () => {
        Bicicleta.allBicis = [];
        expect(Bicicleta.allBicis.length).toBe(0);

        var aBici = new Bicicleta(1, 'verde', 'urbana');
        Bicicleta.add(aBici);
        expect(Bicicleta.allBicis.length).toBe(1);
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(0);
    })
});
*/
